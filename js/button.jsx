import React, {Component, PropTypes} from 'react'

class Button extends Component {

  render () {
    return <div>
      <button style={{color:'red'}} onClick={e => this.handleClick(e)}>нажми меня</button>
    </div>
  }

  handleClick (e) {
    //this.props.onAddClick('text')
    this.props.onPresentationLoad(this.props.page)
  }
}

Button.propTypes = {
  onAddClick: PropTypes.func.isRequired,
  onPresentationLoad: PropTypes.func.isRequired,
  page: PropTypes.number
}

export default Button
