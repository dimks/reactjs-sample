import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import HelloMessage from '../HelloMessage';
import TopNav from '../layouts/TopNav';
import Counter from '../counter';
import Button from '../button';
import {testAction, getPresentations} from '../actions/actions'

const menuItems = [
  {
    'id': 1,
    'name': 'Mike',
    'url': '/mike'
  }, {
    'id': 2,
    'name': 'Donnie',
    'url': '/donnie'
  }, {
    'id': 3,
    'name': 'Raph',
    'url': '/raph'
  }, {
    'id': 4,
    'name': 'Leo',
    'url': '/leo'
  }
]

class App extends React.Component {
  render () {
    // Injected by connect() call:
    const {dispatch, items, presentations} = this.props

    return <div>
      <Counter items={items}></Counter>
      <Button onAddClick={text => dispatch(testAction(text))} page={presentations.page} onPresentationLoad={data => dispatch(getPresentations(data))}></Button>
    </div>;
  }
}

App.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({text: PropTypes.string.isRequired, status: PropTypes.number.isRequired}))
}

function select(state) {
  return {items: state.items, presentations: state.presentations}
}

// Wrap the component to inject dispatch and state into it
export default connect(select)(App)
