import React from 'react';

export default class HelloMessage extends React.Component {
  render() {
      return <div>{this.props.name}</div>;
  }
}
