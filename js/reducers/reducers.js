import {
  combineReducers
}
from 'redux'
import {
  REQUEST_PRESENTATIONS, RECEIVE_PRESENTATIONS
}
from '../actions/actions'

function items(state = [], action) {
  switch (action.type) {
    case 'TEST_ACTION':
      return [
        ...state, {
          text: action.text,
          status: action.status
        }
      ]
    default:
      return state;
  }
}

function tmpActions(state = {
  opt1: 1,
  opt2: 2
}, action) {
  return state
}

const presentationsInitState = {
  ready: false,
  loading: false,
  presentations: [],
  count: 0,
  page: 1
}

function presentations(state = presentationsInitState, action) {
  switch (action.type) {
    case REQUEST_PRESENTATIONS:
      return Object.assign({}, state, {
        loading: true
      })
    case RECEIVE_PRESENTATIONS:
      return Object.assign({}, state, {
        loading: false,
        ready: true,
        presentations: action.presentations,
        count: action.count,
        page: action.page + 1
      })
    default:
      return state
  }
}


const appReducers = combineReducers({
  items, tmpActions, presentations
})

export default appReducers
