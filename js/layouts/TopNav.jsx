import React from 'react';
import MenuItem from './MenuItem';

export default class TopNav extends React.Component {
  render () {
    return <nav>
      <ul>
        {this.props.menuItems.map(function(menuItem, i) {
          return (<MenuItem key={i} url={menuItem.url} name={menuItem.name}/>);
        })}
      </ul>
    </nav>;
  }
}
