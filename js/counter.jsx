import React, { Component, PropTypes } from 'react'

class Counter extends Component {
  render() {

    return <div>
      {this.props.items.map(function(item, i) {
        return (<div key={i}>{item.text} - {item.status}</div>);
      })}
    </div>
  }
}

Counter.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    text: PropTypes.string.isRequired,
    status: PropTypes.number.isRequired
  }))
}

export default Counter
