import React, {Component} from 'react'
import {createStore, applyMiddleware, combineReducers, compose} from 'redux'
import {devTools, persistState} from 'redux-devtools'
import createLogger from 'redux-logger'
import {DevTools, DebugPanel, LogMonitor} from 'redux-devtools/lib/react'
import thunk from 'redux-thunk'
import {Provider} from 'react-redux'
import App from './App'
import appReducers from '../reducers/reducers'
import {testAction} from '../actions/actions'

const loggerMiddleware = createLogger()

const finalCreateStore = compose(applyMiddleware(thunk, loggerMiddleware), devTools(), persistState(window.location.href.match(/[?&]debug_session=([^&]+)\b/)))(createStore)

//const reducer = combineReducers(reducers);
const store = finalCreateStore(appReducers)

export default class Root extends Component {
  render () {
    return (
      <div>
        <Provider store={store}>
          <App/>
        </Provider>
        <DebugPanel top right bottom>
          <DevTools store={store} monitor={LogMonitor} visibleOnLoad={true}/>
        </DebugPanel>
      </div>
    )
  }
}
