import fetch from 'isomorphic-fetch';

export const REQUEST_PRESENTATIONS = 'REQUEST_PRESENTATIONS'
export const RECEIVE_PRESENTATIONS = 'RECEIVE_PRESENTATIONS'


export function getPresentations(page) {
  return (dispatch, getState) => {
    page = page || 1
    return dispatch(loadPresentations(page))
  }
}

function requestPresentations(page) {
  return {
    type: REQUEST_PRESENTATIONS,
    page
  }
}

function receivePresentations(page, json) {
  return {
    type: RECEIVE_PRESENTATIONS,
    presentations: json.items,
    count: json.count,
  }
}

export function testAction(text) {
  return {
    type: 'TEST_ACTION',
    text,
    status: 1
  }
}


function loadPresentations(page) {
  return dispatch => {
    dispatch(requestPresentations(page))
    return fetch('http://magnum.ru/api/presentation/getlist?page=' + page)
      .then(req => req.json())
      .then(json => dispatch(receivePresentations(page, json)))
      .catch((error) => console.log(error))
  }
}
