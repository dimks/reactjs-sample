var gulp = require('gulp');
var browserSync = require('browser-sync');

gulp.task('build', function() {
  build();
});

gulp.task('js-build', ['build'], browserSync.reload);

gulp.task('browser-sync', ['js-build'], function() {

  browserSync.init({
    server: {
      baseDir: "./"
    }
  });

  gulp.watch(['js/**/*.jsx', 'js/**/*.js'], ['js-build']);

});


function build() {
  var browserify = require('browserify');
  var babelify = require('babelify');
  var source = require('vinyl-source-stream');

  return browserify({
      entries: './js/index.jsx',
      extensions: ['.jsx', '.js'],
      debug: true
    })
    .transform("babelify", {
      presets: ["es2015", "react"]
    })
    .bundle()
    .on('error', function(err) { console.error(err); this.emit('end'); })
    // Передаем имя файла, который получим на выходе, vinyl-source-stream
    .pipe(source('app.js'))
    .pipe(gulp.dest('./build/'));
}
