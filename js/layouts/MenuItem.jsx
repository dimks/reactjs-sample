import React from 'react';

export default class MenuItem extends React.Component {
  render () {
    return <li><a key={this.props.key} href={this.props.url}>{this.props.name}</a></li>;
  }
}
